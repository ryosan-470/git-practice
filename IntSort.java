import java.util.Scanner;

public class IntSort {
    private static int x[];
    private static int length;         // 配列の長さ
    private static int compare_count;  // 交換回数
 
   public IntSort(int x[], int length) {
	this.x = x;
	this.length = length;
	compare_count = 0;
    }
    
    /* 交換 */
    public static void swap(int x[], int i, int j) {
	compare_count++;
	int tmp = x[i];
	x[i] = x[j];
	x[j] = tmp;
    }
    /* バブルソート */
    public static void bubbleSort() {
	for (int i = 0; i < length -1; i++) {
	    for (int j = i+1; j < length; j++) {
		if (x[i] > x[j])
		    swap(x,i,j);
	    }
	}
    }

    /* クイックソート leftからrightまでを分割する */
    public static void quickSort(int left, int right) {
	int pl = left;
	int pr = right;
	int pivot = x[(pl + pr) / 2];   // pivot
	
	do {
	    while (x[pl] < pivot) pl++;
	    while (x[pr] > pivot) pr--;
	    if (pl <= pr)
		swap(x, pl++, pr--);
	} while (pl <= pr);

	if (left < pr)
	    quickSort(left,pr);
	if (pl < right)
	    quickSort(pl,right);
    }

    // 配列の中身を表示する
    public static void display() {
	for (int i = 0; i < length; i++) 
	    System.out.print(x[i]+" ");
	System.out.println();
    }

    // 交換回数を得る
    public static int getCompareCount() {
	return compare_count;
    }

    public static void main(String[] args) {
	Scanner sc = new Scanner(System.in);

	if (args.length != 0) {
	    // 入力
	    int n = sc.nextInt();
	    int arrays[] = new int[n];
	    
	    IntSort s = new IntSort(arrays, n);

	    for (int i = 0; i < n; i++) 
		arrays[i] = sc.nextInt();

	    if (args[0].equals("-b")) {
		s.bubbleSort();
	    } else if (args[0].equals("-q")) {
		s.quickSort(0,n-1);
	    }

	    s.display();
	    System.out.println(s.getCompareCount());

	} else {
	    System.out.println("Usage java IntSort [-b] [-q]");
	    System.exit(1);
	}
	
    }
}
